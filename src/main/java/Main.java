import entities.Pazymys;
import entities.Studentas;
import services.StudentasService;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        StudentasService studentasService = new StudentasService();

        //Priskiriame duoemnys is DB kolekcijai
        Map<Integer, Studentas> mapas = studentasService.gautiDuomenis();

        //Išrikiuojame kolekcija
        List<Studentas> studentai = mapas.values().stream().sorted(Comparator.comparing(Studentas::getPavarde).thenComparing(Studentas::getVardas)).collect(Collectors.toList());


        //Patikrinam ar veikia
        for(Studentas n : studentai) {
            System.out.println(n);
        }

        Double vidurkis = vidurkis(mapas);
        System.out.println("Bendras studentų vidurkis: " + vidurkis);

    }

    /**
     * Apskaičiuoja studentų bendrą vidurkį
     * @param mapas kolekcija su studentų duomenimis
     * @return gražina apskaiciuotą visų studentų vidurkį
     */
    public static Double vidurkis(Map<Integer, Studentas> mapas) {
        Integer suma = 0;
        Integer count = 0;
        for(Studentas n : mapas.values()) {
            for(Pazymys d : n.getPazymiai()) {
                suma += d.getPazymys();
                count++;
            }
        }
        return suma*1.0/count;
    }
}
