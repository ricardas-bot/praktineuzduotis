package entities;

import java.time.LocalDate;

/**
 *  Klasė atitinkanti pazymių lentelę duomenų bazėje.
 */
public class Pazymys {
    private int id;
    private Studentas studentas;
    private LocalDate data;
    private int pazymys;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Studentas getStudentas() {
        return studentas;
    }

    public void setStudentas(Studentas studentas) {
        this.studentas = studentas;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public int getPazymys() {
        return pazymys;
    }

    public void setPazymys(int pazymys) {
        this.pazymys = pazymys;
    }

    @Override
    public String toString() {
        return "Pazymys{" +
                "pazymys=" + pazymys +
                '}';
    }
}
