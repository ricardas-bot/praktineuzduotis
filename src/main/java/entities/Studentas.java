package entities;

import java.util.Collection;
import java.util.Objects;

/**
 *  Klasė atitinkanti stundentų lentelę duomenų bazėje.
 */
public class Studentas {
    private int id;
    private String vardas;
    private String pavarde;
    private String elPastas;
    private Collection<Pazymys> pazymiai;

    public Collection<Pazymys> getPazymiai() {
        return pazymiai;
    }

    public void setPazymiai(Collection<Pazymys> pazymiai) {
        this.pazymiai = pazymiai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public String getElPastas() {
        return elPastas;
    }

    public void setElPastas(String elPastas) {
        this.elPastas = elPastas;
    }

    @Override
    public String toString() {
        return "Studentas{" +
                "id=" + id +
                ", vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", pazymiai=" + pazymiai +
                '}';
    }
}

