package services;

import entities.Pazymys;
import entities.Studentas;

import java.sql.*;
import java.util.*;


public class StudentasService {
    /**
     * Sukuria studentas klasės objektą su duomenų bazės duomenimis
     * @param resultSet duomenys iš duomenų bazės, pagal SQL užklausa
     * @return gražina studentas objektą, suformuotą, pagal duomenų bazės duoemnis
     * @throws SQLException meta klaida, jei yra kažkas blogai su duomenų baze
     */
    private Studentas mapStudentasFromResult(ResultSet resultSet) throws SQLException {
        Studentas studentas = new Studentas();

        studentas.setId(resultSet.getInt("id"));
        studentas.setVardas(resultSet.getString("vardas"));
        studentas.setPavarde(resultSet.getString("pavarde"));
        studentas.setElPastas(resultSet.getString("el_pastas"));

        if (resultSet.getDate("data") != null) {
            if(studentas.getPazymiai() == null) {
                 studentas.setPazymiai(new ArrayList<>());
            }
                Pazymys pazymys = mapPazymysFromResult(resultSet);
                studentas.getPazymiai().add(pazymys);

        }

        return studentas;
    }

    /**
     * Sukuria pazymys klasės objektą su duomenų bazės duomenimis
     * @param resultSet duomenys iš duomenų bazės, pagal SQL užklausa
     * @return gražina sukurtą pazymys objektą
     * @throws SQLException meta klaida, jei yra kažkas blogai su duomenų baze
     */
    private Pazymys mapPazymysFromResult(ResultSet resultSet) throws SQLException {
        Pazymys pazymys = new Pazymys();

        pazymys.setData(resultSet.getDate("data").toLocalDate());
        pazymys.setPazymys(resultSet.getInt("pazymys"));

        return pazymys;
    }

    /**
     * Užpildome kolekcija su duomenimis is duomenų bazės.
     * @return gražiname užpildyta kolekcija.
     */
    public Map<Integer, Studentas> gautiDuomenis() {
        Map<Integer, Studentas> mapas = new HashMap<>();
        String SQL = "SELECT * FROM studentai \n" +
                "JOIN pazymiai ON studentas_id = studentai.id";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dienynas?serverTimezone=Europe/Vilnius&useUnicode=true&characterEncoding=UTF-8",
                    "root", "12345");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL);
            while(resultSet.next()) {
                if(mapas.containsKey(resultSet.getInt("id"))) {
                    mapas.get(resultSet.getInt("id")).getPazymiai().add(mapPazymysFromResult(resultSet));
                } else {
                    mapas.put(resultSet.getInt("id"), mapStudentasFromResult(resultSet));
                }
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("Kazkas atsitiko");
            e.printStackTrace();
        }
        return mapas;
    }


}
